package fr.afpa.controller;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Sofiane + Elvis
 *
 * modele mvc
 * classe de controle d'authentification
 * 
 */
public class ControlAuthentification {
	FileReader fr=null;
	BufferedReader br=null;
	String source= "" ;
	/**
	 * @author Sofiane + Elvis
	 *
	 * méthode de controle d'authentification
	 * lien vers le fichier racine
	 * ouverture fichier
	 * lecture et vérification
	 * try catch pour fermer le fichier en cas de probleme , fichier non fermer avant autre
	 */
	public void controlAuth() {
	try {
		fr= new FileReader("C:\\ENV\\BDD\\config.txt");
		br =new BufferedReader(fr);
		source= br.readLine();
				
	} catch (Exception e) {
		System.out.println("Erreur"+e);

	}finally {
		if (br!=null) {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
				
			}
		}
	}
	/**
	 * @author Sofiane + Elvis
	 *
	 * demande de login 
	 * demande de mot de passe
	 * message de bienvenue
	 * renvoie de message si log ou mdp incorect
	 */
	String [] user=source.split("/");
	Scanner in = new Scanner(System.in);
	System.out.println("Veuillez saisir le login");
	String login= in.nextLine();
	System.out.println("Veuillez saisir le mot de passe");
	String passWord= in.nextLine();
	if (login.equals(user[0]) && passWord.equals(user[1]) ) {
		System.out.println( "<<<< BIENVENUE >>>>>");
	}else {
		System.out.println("login et/ou mot de passe incorrect");
	}
}
}
