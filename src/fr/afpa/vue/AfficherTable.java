package fr.afpa.vue;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.beans.Table;

public class AfficherTable {
	private Table table;
	/**
	 * @author Sofiane + Elvis
	 *methode affichage 
	 *
	 */
	public AfficherTable(Table trestres,ArrayList<String>colonne) {
		
		this.table = new Table("table", colonne);
	}

	public static void afficheTable(Table nomTable) {
		String table = nomTable.getNomTable();
		ArrayList<String>colonnes = nomTable.getColonne();

		int longChoix = table.length();

		for (String colonne : colonnes) {
			if (colonne.length() > longChoix) {
				longChoix = colonne.length();
			}
		}

		StringBuilder s = new StringBuilder();

		appendDashes(longChoix, s);
		appendLine(table, longChoix, s);
		appendDashes(longChoix, s);
		for (String colonne : colonnes) {
			appendLine(colonne, longChoix, s);
		}
		appendDashes(longChoix, s);

		System.out.println(s + "\n");
	}
/*
 * methode pour assembler des table ensemble
 */
	public static void AssemTable(Table one) {
		String table = one.getNomTable();
		ArrayList<String> colonne1 = one.getColonne();
		int longChoix = table.length();
		for (String colonne : colonne1) {
			if (colonne.length() > longChoix) {
				longChoix = colonne.length();	
			}
		}
		StringBuilder s = new StringBuilder();
	
		appendDessusDessous(longChoix, s);
		
		appendCote(longChoix, s);
		apTitletable(table, longChoix, s);
		s.append("\n");
		appendDessusDessous(longChoix, s);

		for (String colonne : colonne1) {
			appendLine(colonne, longChoix, s);			
		}
		appendDashes(longChoix, s);
		System.out.println(s);
	
}
	/*
	 * methode pour afficher une table
	 */
public static void affOneTable(Table seul) {
	String table = seul.getNomTable();
	ArrayList<String> colonne1 = seul.getColonne();
	int longChoix = table.length();
	for (String colonne : colonne1) {
		if (colonne.length() > longChoix) {
			longChoix = colonne.length();	
		}
	}
	StringBuilder s = new StringBuilder();
	
	appendDessusDessous(longChoix, s);
	appendCote(longChoix, s);
	apTitletable(table, longChoix, s);
	appendCote(longChoix, s);
	s.append("\n");
	appendDessusDessous(longChoix, s);
	for (String colonne : colonne1) {
		appendLine(colonne, longChoix, s);
		appendCote(longChoix, s);
	s.append("\n");
	}
	appendDashes(longChoix, s);
	
	System.out.print(s);
}
/**
 * @author Sofiane + Elvis
 *
 *methode pour affiche le nom de la table
 */
	public static void apTitletable(String value, int maxSize, StringBuilder builder) {
		builder.append(" ");
		builder.append(value);
		for (int i = 0; i < maxSize - value.length() + 1; i++) {
			builder.append(' ');
		}
	}
	/**
	 * @author Sofiane + Elvis
	 *
	 *methode pour la construction de coté 
	 */
	public static void appendCote(int maxSize, StringBuilder builder) {
		builder.append('|');

	}
	/**
	 * @author Sofiane + Elvis
	 *méthode pour la création de ligne en dessus ou en dessous
	 *la méthode prend en charge la longueur la plus longue entre les champs de la même colonne
	 *
	 */
	public static void appendDessusDessous(int maxSize, StringBuilder builder) {
		builder.append('+');
		for (int i = 2; i < maxSize + 4; i++) {
			builder.append('-');
		}
		builder.append('+');
		builder.append("\n");
	}
	/**
	 * @author Sofiane + Elvis
	 *
	 *méthode pour la création de traits
	 *la méthode prend en charge la longueur la plus longue entre les champs de la même colonne
	 */
	public static void appendtrait(int maxSize, StringBuilder builder) {
		for (int i = 0; i < maxSize + 2; i++) {
			builder.append('-');
		}
	}
	/**
	 * @author Sofiane + Elvis
	 *
	 *methode pour ajout du symbole + en coté doit ou gauche
	 */
	public static void appendMore(int maxSize, StringBuilder builder) {
		builder.append('+');

	}
	/**
	 * @author Sofiane + Elvis
	 *
	 *methode pour afficher une ligne complete avec + et -
	 *la méthode prend en charge la longueur la plus longue entre les champs de la même colonne
	 */
	public static void appendDashes(int maxSize, StringBuilder builder) {
		builder.append('+');
		for (int i = 0; i < maxSize + 2; i++) {
			builder.append('-');
		}
		builder.append('+');

	}
	/**
	 * @author Sofiane + Elvis
	 *méthode pour la création le ligne 
	 *
	 */
	public static void appendLine(String value, int maxSize, StringBuilder builder) {
		builder.append('|');
		builder.append(' ');
		builder.append(value);
		for (int i = 0; i < maxSize - value.length() + 1; i++) {
			builder.append(' ');
			
		}
		
	}
	/**
	 * @author Sofiane + Elvis 
	 * méthode pour afficher deux tables et colonnes
	 */
	public static void affichDeuxTables(Table one, Table two) {
		AssemTable(one);
		AssemTable(two);
	}



}
