package fr.afpa.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Update {
	/**
	 * @author Sofiane + Elvis
	 *methode pour la mise à jour
	 */
	public void update() {
		Scanner in = new Scanner(System.in);
		String str = in.nextLine();
		Pattern p = Pattern.compile("(?i)(update)(?-i)[a-z_A-Z]*\\=[ ]?\\'[a-z_A-Z]*\\'\\;");
		Matcher m = p.matcher(str);
		/**
		 * @author Sofiane + Elvis
		 *regex si matches lecture de choix pour la modification
		 *ouverture et fermeture fichier
		 *
		 */
		if (m.matches()) {
			int b = str.indexOf("[");
			System.out.println(str.substring(12, b));
			File file = new File("C:/ENV/BDD/" + str.substring(12, b) + ".txt");
			// StringBuffer s=new StringBuffer(str);
			if (file.exists()) {
				FileReader fr = null;
				BufferedReader br = null;
				try {
					fr = new FileReader(file);
					br = new BufferedReader(fr);
					System.out.println("");
					/**
					 * @author Sofiane + Elvis
					 *
					 *definition en fonction du regex du demarage de lecture 
					 *de la saisie utilisateur pour update
					 */
					int taille = br.readLine().split(";").length;
					int c = str.indexOf("(");
					if (str.substring(c + 1, str.length() - 2).split(",").length == taille) {

						FileWriter myWriter = new FileWriter("C:/ENV/BDD/" + str.substring(12, b) + ".txt", true);
						
						String champs = str.substring(c + 1, str.length() - 2).replace(",", ";").replace("'", "");
						myWriter.write( champs);
						myWriter.close();
						/**
						 * @author Sofiane + Elvis
						 * envoie de message erreur ou succes
						 */
						
						System.out.println("Ligne modifiée avec succés ");
					} 
				} catch (IOException e) {

					System.out.println("Erreur !");
					e.printStackTrace();

				} finally {
					// myWriter.close();
				}

			}
		} else {
			System.out.println("Erreur syntaxe!!");
			System.out.println("veuillez réspecter la syntaxe : UPDATE nomTableSETnom_du_champ= 'nouvelle valeur'; ");
		}
	}
}