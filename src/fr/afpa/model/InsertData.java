package fr.afpa.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InsertData {
	/**
	 * @author Sofiane + Elvis
	 *
	 * methode pour inserer des colonnes
	 * 
	 */
	public void insert() {
		/**
		 * @author Sofiane + Elvis
		 *
		 * regex verifie que la saisie entrée corespond au attente pour l'insertion 
		 * avec un modele de renvoie si saisie incorect
		 */
	Scanner  in = new Scanner(System.in);
	String str= in.nextLine();
	Pattern p = Pattern.compile("(?i)(insert into)(?-i) [a-z_A-Z]+ VALUES\\(('[a-z_A-Z]{1,25}',)*'[a-z_A-Z]{1,25}'\\);"); 
	Matcher m = p.matcher(str);
	/**
	 * @author Sofiane + Elvis
	 *
	 * matches verifie que le regex corespond 
	 * si valide lit de ou comencer la lecture pour l'insertion
	 */
	if (m.matches()) {
		int b= str.indexOf(" VALUES");
		//System.out.println(str.substring(12, b));
		File file = new File("C:/ENV/BDD/"+str.substring(12, b)+".cda");		
			
		if (file.exists()) {
			FileReader fr=null;
			BufferedReader br=null;
			try {				
			fr = new FileReader(file);	
			br= new BufferedReader(fr);
				
			int taille=br.readLine().split(";").length;
			int c= str.indexOf("(");
			if (str.substring(c+1, str.length()-2).split(",").length==taille) {
				/**
				 * @author Sofiane + Elvis
				 *
				 * ouverture et ecriture dans le fichier
				 * avec extensions .cda
				 * fermeture fichier
				 */
			FileWriter myWriter = new FileWriter("C:/ENV/BDD/"+str.substring(12, b)+".cda", true);
			
			String champs= str.substring(c+1, str.length()-2).replace(",", ";").replace("'", "");			
				myWriter.write(System.lineSeparator() +champs);
				myWriter.close();
				/**
				 * @author Sofiane + Elvis
				 *
				 * ernvoie de message en cas erreur ou de succes
				 */
			System.out.println("Ligne ajoutée avec succès ");
			}else {
				System.out.println("ERREUR, veuillez vérifier le nombre de champs!!");
			}
			} catch (IOException e) {
			
			System.out.println("Erreur !");
			e.printStackTrace();
		
			}finally {
			//myWriter.close();
			}
			
		}else {
				System.out.println("Erreur CETTE TABLE n'existe pas !!");
		}
	}else {
		System.out.println("Erreur syntaxe!!");
		System.out.println("veuillez réspecter la syntaxe : INSERT INTO nomTable VALUES('valeur 1', 'valeur 2');");
}
}
}
