package fr.afpa.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SelectData {
	/**
	 * @author Sofiane + Elvis
	 *
	 * méthode pour calculer le nombre de ligne
	 */
	
	private int countLines(File file) throws IOException {
	    int lines = 0;

	    FileInputStream fis = new FileInputStream(file);
	    byte[] buffer = new byte[8*1024]; // BUFFER_SIZE = 8 * 1024
	    int read;

	    while ((read = fis.read(buffer)) != -1) {
	        for (int i = 0; i < read; i++) {
	            if (buffer[i] == '\n') lines++;
	        }
	    }

	    fis.close();

	    return lines;
	}
	/**
	 * @author Sofiane + Elvis
	 *
	 * méthode pour select *
	 */
	
	
	public void selectAll() {
		/**
		 * @author Sofiane + Elvis
		 *
		 * vérification du regex pour cela corespond au modéle choisi 
		 */
		Scanner in = new Scanner(System.in);
		String str= in.nextLine();
		Pattern p = Pattern.compile("(?i)(select \\* from)(?-i) [a-z_A-Z]+;"); 
		Matcher m = p.matcher(str);
		/**
		 * @author Sofiane + Elvis
		 *
		 * verifie si le modéle matche
		 * 
		 */
		if (m.matches()) {
			int b= str.indexOf("");
			
			File file = new File("C:/ENV/BDD/"+str.substring(14, str.length()-1)+".cda");		
					
			if (file.exists()) {
				FileReader fr=null;
				BufferedReader br=null;
				try {
					fr = new FileReader(file);	
					br= new BufferedReader(fr);				
					//String [] tab= br.readLine().split(";");
//					for (int j = 0; j < tab.length; j++) {
//					//System.out.println("+"+String.join("", Collections.nCopies(25, "-"))+"+");
//					
//					System.out.println("| "+tab[j]+String.join("", Collections.nCopies(25-tab[j].length(), " "))+"|");
//					
//					System.out.println("+"+String.join("", Collections.nCopies(25, "-"))+"+");
//					}
					
					for (int i= 0; i < this.countLines(file)+1; i++) {
						String [] tab= br.readLine().split(";");
					
						System.out.print("| "+tab[0]+String.join("", Collections.nCopies(25-tab[0].length(), " "))+"|");
					for (int j = 1; j < tab.length; j++) {
						System.out.print(tab[j]+String.join("", Collections.nCopies(25-tab[j].length(), " "))+"|");
						
					}
						System.out.println();
					
					}
					

				} catch (IOException e) {
				
					System.out.println("Erreur !");
					e.printStackTrace();
			
				}finally {
				//myWriter.close();
				}
				/**
				 * @author Sofiane + Elvis
				 *
				 * renvoie d'erreur le cas échéant
				 */
			}else {
					System.out.println("Erreur CETTE TABLE n'existe pas !!");
			}
		}else {
			System.out.println("Erreur syntaxe!!");
			System.out.println("veuillez réspecter la syntaxe : SELECT * FROM nomTable;");
	}
	
	}
	
	/**
	 * @author Sofiane + Elvis
	 *
	 * méthode select par colonne
	 */
	
	public void selectColumn() {
		
		Scanner in = new Scanner(System.in);
		String str= in.nextLine();
		Pattern p = Pattern.compile("(?i)(select)(?-i) ([a-z_A-Z]+,)*[a-z_A-Z]+ FROM [a-z_A-Z]+;"); 
		Matcher m = p.matcher(str);
		
		if (m.matches()) {
			int b= str.indexOf("FROM");
			
			File file = new File("C:/ENV/BDD/"+str.substring(b+5, str.length()-1)+".cda");		
			
			if (file.exists()) {
				FileReader fr=null;
				BufferedReader br=null;
				try {
					int indice =0;
					fr = new FileReader(file);	
					br= new BufferedReader(fr);				
					String [] tab= br.readLine().split(";");		
					String [] champs= str.substring(7,b-1).split(",");
					/**
					 * @author Sofiane + Elvis
					 *
					 * affichage des données si le champs existe
					 */
					for (int k = 0; k < champs.length; k++) {
						if(Arrays.asList(tab).contains(champs[k])) {
							for (int i = 0; i < tab.length; i++) {
								if (champs[k].equals(tab[i])) {
									indice =i;
									System.out.println("+ "+String.join("", Collections.nCopies(25, "-"))+" +");
									System.out.println("| "+tab[i]+String.join("", Collections.nCopies(25-tab[i].length(), " "))+" |");
									System.out.println("+ "+String.join("", Collections.nCopies(25, "-"))+" +");

								} 
							}
							for (int i = 1; i < this.countLines(file)+1; i++) {
									tab= br.readLine().split(";");
								for (int j = 0; j < tab.length; j++) {
									if (j==indice) {
									System.out.print("| "+tab[j]+String.join("", Collections.nCopies(25-tab[j].length(), " "))+" |");
								}}
								System.out.println();
							}
							System.out.println("+ "+String.join("", Collections.nCopies(25, "-"))+" +");
						}else {
							/**
							 * @author Sofiane + Elvis
							 *
							 * renvoie d'un message d'erreur le cas échéant
							 */
							System.out.println("le champs '"+str.substring(7,b-1) +"' n'existe pas dans la table!!");
							break;
						}
					}
					
				} catch (IOException e) {
				
				System.out.println("Erreur !");
				e.printStackTrace();
			
				}finally {
				//myWriter.close();
				}
				
			}else {
					System.out.println("Erreur CETTE TABLE n'existe pas !!");
			}
		}else {
			System.out.println("Erreur syntaxe!!");
			System.out.println("veuillez réspecter la syntaxe : SELECT * FROM nomTable;");
	}
	
}	
}
