package fr.afpa.model;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CreateTable {
	/**
	 * @author Sofiane + Elvis
	 *
	 * methode pour la creation de table
	 */
	public void Create() {
		/**
		 * @author Sofiane + Elvis
		 *
		 * regex pour vérifier que le champs entrée corespond au attente de requete
		 * si matches creation du fichier .txt avec nom corespondant au nom de la table
		 */
	Scanner  in = new Scanner(System.in);
	String str= in.nextLine();
	Pattern p = Pattern.compile("(?i)(create table)(?-i) [a-z_A-Z]+\\(([a-z_A-Z]+,)*[a-z_A-Z]+\\);"); 
	
	Matcher m = p.matcher(str);
	
	if (m.matches()) {
		int b= str.indexOf("(");
		File file = new File("C:/ENV/BDD/"+str.substring(13, b)+".cda");		
		/**
		 * @author Sofiane + Elvis
		 *
		 * verification si fichier existe ou pas
		 * 
		 */
		if (!file.exists()) {	
			try {				
					
			FileWriter myWriter = new FileWriter("C:/ENV/BDD/"+str.substring(13, b)+".cda", true);
					
			String champs= str.substring(b+1, str.length()-2).replace(",", ";");			
				myWriter.write(champs);
				myWriter.close();
		
			System.out.println("Table créée avec succès");
		
			} catch (IOException e) {
			
			System.out.println("Erreur !");
			e.printStackTrace();
		
			}finally {
			//myWriter.close();
			}
		}else {
				System.out.println("Erreur CETTE TABLE existe dèja !!");
		}
	}else {
		System.out.println("Erreur syntaxe!!");
		System.out.println("veuillez réspecter la syntaxe : CREATE TABLE nomTable(colonne1, colonne2,colonne3);");
}
}
}
