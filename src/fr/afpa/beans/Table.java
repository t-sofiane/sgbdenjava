package fr.afpa.beans;

import java.util.ArrayList;
import java.util.List;

public class Table {
	/**
	 * @author Sofiane + Elvis
	 *
	 * Création de la table
	 * ArrayList pour les colonnes
	 */
	private String nomTable;
	private ArrayList<String>colonne;

	public Table(String nomTable, ArrayList<String>colonne) {
		this.nomTable = nomTable;
		this.colonne = colonne;
	}

	public String getNomTable() {
		return nomTable;
	}

	public void setNomTable(String nomTable) {
		this.nomTable = nomTable;
	}

	public ArrayList<String> getColonne() {
		return colonne;
	}

	public void setColonne(ArrayList<String>colonne) {
		this.colonne = colonne;
	}
	/**
	 * @author Sofiane + Elvis
	 *
	 * toString
	 * redefinition pour afficher
	 */
	@Override
	public String toString() {
		return  nomTable + " " + colonne ;
	}
	
}
